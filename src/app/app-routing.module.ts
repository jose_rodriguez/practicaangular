import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaUsuariosComponent } from './usuario/lista-usuarios/lista-usuarios.component';
import { FichaUsuarioComponent } from './usuario/ficha-usuario/ficha-usuario.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'usuarios', component: ListaUsuariosComponent},
  {path: 'usuario/:id', component: FichaUsuarioComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
