import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { CabeceraComponent } from './comun/cabecera/cabecera.component';
import { CuerpoComponent } from './comun/cuerpo/cuerpo.component';
import { PieComponent } from './comun/pie/pie.component';
import { AddUsuarioComponent } from './usuario/add-usuario/add-usuario.component';
import { ListaUsuariosComponent } from './usuario/lista-usuarios/lista-usuarios.component';
import { EditarUsuarioComponent } from './usuario/editar-usuario/editar-usuario.component';
import { FichaUsuarioComponent } from './usuario/ficha-usuario/ficha-usuario.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CabeceraComponent,
    CuerpoComponent,
    PieComponent,
    AddUsuarioComponent,
    ListaUsuariosComponent,
    EditarUsuarioComponent,
    FichaUsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
